import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import Vant from 'vant';
import 'vant/lib/index.css';

import axios from 'axios'
axios.defaults.baseURL = 'http://39.102.77.255:8686'; // http://39.102.77.255:8686
Vue.prototype.$axios = axios;


Vue.use(Vant);
Vue.config.productionTip = false
Vue.use(ElementUI);
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
