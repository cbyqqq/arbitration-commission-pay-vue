import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [

  {
    path:'/:id',
    component: () => import('@/views/Progress/poy2/pay2.vue'),
    hidden: false
  },
  {
    path: '/notFound',
    name:"NotFound",
    component: () => import('@/views/Progress/404.vue'),
    hidden: true
  },
  {
    path: '/immediatePayment',
    name:"ImmediatePayment",
    component: () => import('@/views/Progress/immediatePayment.vue'),
    hidden: true
  },
  {
    path: '/paySuccess',
    name:"PaySuccess",
    component: () => import('@/views/Progress/poy2/paySuccess.vue'),
    hidden: true
  },
  {
    path: '/pay',
    name:"Pay",
    component: () => import('@/views/Progress/poy2/pay.vue'),
    hidden: true
  },
  {
    path: '/payError',
    name:"PayError",
    component: () => import('@/views/Progress/poy2/payError.vue'),
    hidden: true
  },
  {
    path: '/payInvalid',
    name:"PayInvalid",
    component: () => import('@/views/Progress/poy2/payInvalid.vue'),
    hidden: true
  },
]

const router = new VueRouter({
  // mode: 'history',
  // base: process.env.BASE_URL,
  routes
})

export default router
